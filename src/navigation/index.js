import React, {useEffect, useState} from 'react';
import NetInfo from '@react-native-community/netinfo';
import {View} from 'react-native';
import {SplashScreen} from '../components';
import NetworkConnectionModal from '../components/NetworkConnectionModal';
import AppNavigator from './appNavigator';
import {useDispatch} from 'react-redux';
import {setNetworkInfo, setUserCountry} from '../store/app/app.actions';
import styles from './style';
import { deviceLocalCode } from '../utils/helpers';
function RootNavigator() {
  const [isFetchNetwork, setIsFetchNetwork] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setUserCountry({userCountry:deviceLocalCode}))
    const unsubscribe = NetInfo.addEventListener(state => {
      dispatch(setNetworkInfo({networkInfo: state}));
      setIsFetchNetwork(false);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  return isFetchNetwork ? (
    <SplashScreen />
  ) : (
    <View style={styles.containerApp}>
      <NetworkConnectionModal />
      <AppNavigator />
    </View>
  );
}

export default RootNavigator;
