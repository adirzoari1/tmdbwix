import React from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import {Movies, SearchMovies, Watchlist} from '../screens';
import styles from './style';
import * as theme from '../utils/theme';
import {moviesWatchlistSelector} from '../store/watchlist/watchlist.selectors';


const Tab = createBottomTabNavigator();

const Stack = createStackNavigator();
export const StackMovies = () => (
  <Stack.Navigator
    screenOptions={{
      headerShown: false,
    }}>
    <Stack.Screen name="Movies" component={Movies} />
    <Stack.Screen
      name="SearchMovies"
      component={SearchMovies}
      options={{tabBarVisible: false}}
    />
  </Stack.Navigator>
);

function AppNavigator() {
    const moviesWatchlist = useSelector(moviesWatchlistSelector);

    return (
        <NavigationContainer>
        <Tab.Navigator
          screenOptions={({route}) => ({
            tabBarIcon: ({focused, color, size}) => {
              if (route.name === 'Movies') {
                return (
                  <Icon
                    name={focused ? 'videocam' : 'videocam-outline'}
                    size={size}
                    color={color}
                  />
                );
              } else if (route.name === 'Watchlist') {
                return (
                  <Icon
                    name={focused ? 'bookmark' : 'bookmark-outline'}
                    size={size}
                    color={color}
                  />
                );
              }
            },
          })}
          tabBarOptions={{
            activeTintColor: theme.colors.purple,
            inactiveTintColor: theme.colors.black_o40,
            tabStyle: styles.tabStyle,
            style: styles.tabBarStyle,
          }}>
          <Tab.Screen name="Movies" component={StackMovies} />
          <Tab.Screen
            name="Watchlist"
            component={Watchlist}
            options={{
              ...(moviesWatchlist?.length && {
                tabBarBadge: moviesWatchlist.length,
                tabBarBadgeStyle: styles.tabBarBadgeStyle,
              }),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    )
}

export default AppNavigator
