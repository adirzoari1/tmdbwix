import React, { useState, useEffect, useRef, useCallback } from "react";

const useDebounce = (func, wait) => {
  const timeout = useRef();

  return useCallback(
    (...args) => {
      const runFunc = () => {
        clearTimeout(timeout.current);
        func(...args);
      };
      clearTimeout(timeout.current);
      timeout.current = setTimeout(runFunc, wait);
    },
    [func, wait]
  );
};
export default useDebounce