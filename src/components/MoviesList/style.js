import {StyleSheet} from 'react-native';
import {windowHeight, windowWidth} from '../../utils/helpers';
import * as theme from '../../utils/theme';

const style = StyleSheet.create({
  viewMoviesList: {
      flex:1,
  },
  contentFlatList: {
    flexGrow: 1,
    padding: 20,
  },
  viewHeaderList:{

  },
  textHeaderList:{
    fontFamily:theme.fonts.bold,
    color:theme.colors.white,
    fontSize:15,
  },
  viewShareMovieButton:{
    position:'absolute',
    right:'50%',
    top:-20,
    zIndex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  viewShareIcon:{
    width:60,
    height:60,
    backgroundColor:theme.colors.purple,
    borderRadius:30,
    justifyContent:'center',
    alignItems:'center'
  },
  flatList:{
    marginBottom:30,
  }
 
});

export default style;
