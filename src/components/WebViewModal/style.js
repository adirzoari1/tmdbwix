import {StyleSheet} from 'react-native';
import {windowHeight, windowWidth} from '../../utils/helpers';
import * as theme from '../../utils/theme';
const style = StyleSheet.create({
  viewModal:{
    justifyContent: 'flex-end',
    margin: 0,
  },
  content:{
    backgroundColor: theme.colors.white,
    padding: 20,
    borderTopRightRadius:10,
    borderTopLeftRadius:10,
    height:500
  },
  webViewStyle:{
    height: 500,
  }
});

export default style;
