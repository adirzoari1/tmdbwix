import React from 'react';
import {View, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Button } from '..';
import * as theme from '../../utils/theme';

function SearchBar({
  styleViewSearch,
  styleViewSearchInput,
  styleText,
  colorIconSearch,
  colorCloseIcon,
  value,
  onChangeText,
  onSubmitEditing,
  placeHolderText,
  placeHolderTextColor,
  onPressCloseIcon,
}) {
  return (
    <View style={styleViewSearch}>
      <View style={styleViewSearchInput}>
        <Icon name="search" color={colorIconSearch} size={20}></Icon>
        <TextInput
          value={value}
          onChangeText={onChangeText}
          onSubmitEditing={onSubmitEditing}
          returnKeyType="search"
          autoFocus={true}
          blurOnSubmit
          placeholder={placeHolderText}
          placeholderTextColor={placeHolderTextColor}
          underlineColorAndroid="transparent"
          style={styleText}
        />
      </View>
      <Button onPress={onPressCloseIcon}>
        <Icon name="close-circle" size={30} color={colorCloseIcon}></Icon>
      </Button>
    </View>
  );
}

export default SearchBar;

SearchBar.defaultProps = {
  styleViewSearch: {},
  styleViewSearchInput: {},
  styleText:{},
  colorIconSearch: theme.colors.black,
  colorCloseIcon: theme.colors.black,
  placeHolderTextColor: theme.colors.black,
  value: '',
  placeHolderText: '',
  onChangeText: () => {},
  onPressCloseIcon: () => {},
  onSubmitEditing:()=>{}
};
