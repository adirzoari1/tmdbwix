import React from 'react';
import {ActivityIndicator, View, Text} from 'react-native';
import * as theme from '../../utils/theme'
import styles from './style';
function Spinner({color}) {
  return (
    <View style={styles.container}>
      <ActivityIndicator size="small" color={color} />
    </View>
  );
}


export default Spinner;

Spinner.defaultProps = {
  color:theme.colors.white
}