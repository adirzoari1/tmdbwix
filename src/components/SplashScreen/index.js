import React from 'react';
import { View,Text,Image } from 'react-native';
import styles from './style';

function SplashScreen() {
  return (
    <View style={styles.container}>
      <View style={styles.viewImageLogo}>
        <Image
          source={require('../../assets/images/logo.png')}
          style={styles.imgLogo}
          resizeMode="contain"
        />
      </View>
      <View style={styles.footer}>
        <Text style={styles.textFooter}>Powered By Adir Zoari</Text>
      </View>
    </View>
  );
}

export default SplashScreen;
