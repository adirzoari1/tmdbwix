import {StyleSheet} from 'react-native';
import * as theme from '../../utils/theme';

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    padding: 20,
  },
  viewImageLogo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgLogo: {
    width: 250,
    height: 250,
  },

  footer: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  textFooter: {
    textAlign: 'center',
    fontFamily: theme.fonts.regular,
    fontSize: 12,
  },
});

export default style;
