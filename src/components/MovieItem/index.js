import React, {memo} from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import FastImage from 'react-native-fast-image';
import Config from 'react-native-config';
import styles from './style';
import * as theme from '../../utils/theme';
import {getFullYear} from '../../utils/dates';
import style from './style';
import {Button} from '..';

function MovieItem({item, onPressFav, onPressWatchVideo, isFavorite = false}) {
  function handlPressFav() {
    onPressFav(item);
  }
  function pressWatchVideo() {
    onPressWatchVideo(item);
  }
  return (
    <View style={styles.viewMovieItem} key={item.id}>
      <View style={styles.viewPlayVideo}>
        <Button
          onPress={() => {
            pressWatchVideo(item);
          }}
          style={styles.viewPlayBtnMovie}>
          <Icon name="play" size={15} color={theme.colors.white} />
        </Button>
      </View>
      <FastImage
        style={styles.imageMovie}
        source={{
          uri: `${Config.BASE_IMAGE_URL}/w300/${item.poster_path}`,
          priority: FastImage.priority.normal,
        }}
        resizeMode={FastImage.resizeMode.contain}
      />

      <View style={styles.viewDetails}>
        <Text numberOfLines={1} style={styles.textMovieTitle}>
          {item.title}
        </Text>
        <Text numberOfLines={3} style={styles.textMovieOverview}>
          {item.overview}
        </Text>
        <View style={styles.viewFooterDetails}>
          <View style={styles.viewVoteAverage}>
            <Icon name={'star'} size={20} color={theme.colors.yellow} />
            <Text style={styles.textVoteAverage}>{item.vote_average}</Text>
          </View>
        </View>
      </View>

      <View style={styles.viewFavYear}>
        <Button
          onPress={() => {
            handlPressFav(item);
          }}
          style={styles.favBtnIcon}>
          <Icon
            name={isFavorite ? 'heart' : 'heart-outline'}
            size={20}
            color={theme.colors.purple}
          />
        </Button>
        <View style={styles.viewDate}>
          <Text style={style.textDate}>{getFullYear(item.release_date)}</Text>
        </View>
      </View>
    </View>
  );
}
const areEqual = (prev, next) => {
  prev.isFavorite === next.isFavorite;
};


export default memo(MovieItem,areEqual)
