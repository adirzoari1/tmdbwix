import {StyleSheet} from 'react-native';
import {windowHeight, windowWidth} from '../../utils/helpers';
import * as theme from '../../utils/theme';
const style = StyleSheet.create({
  viewMovieItem: {
    flexDirection: 'row',
    marginVertical: 20,
    justifyContent: 'space-between',
    padding: 10,
  },

  imageMovie: {
    width: 80,
    height: 100,
    borderRadius: 10,
  },
  viewDetails: {
    flex: 1,
    justifyContent: 'space-between',
  },
  textMovieTitle: {
    fontFamily: theme.fonts.semiBold,
    fontSize: 15,
    color: theme.colors.white,
  },
  textMovieOverview: {
    fontFamily: theme.fonts.light,
    fontSize: 12,
    color: theme.colors.grey,
  },
  viewVoteAverage: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textVoteAverage: {
    fontFamily: theme.fonts.bold,
    fontSize: 15,
    color: theme.colors.white,
    marginLeft: 10,
  },
  favBtnIcon: {
    width: 50,
    height: 50,
    borderRadius: 25,
    alignItems: 'flex-end',
  },

  viewFavYear: {
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  viewDate: {
    width: 50,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    backgroundColor: theme.colors.purple,
  },
  textDate: {
    fontFamily: theme.fonts.light,
    fontSize: 12,
    color: theme.colors.white,
  },
  viewFooterDetails: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  viewPlayVideo: {
    zIndex: 1,
    position: 'absolute',
  },
  viewPlayBtnMovie: {
    backgroundColor: theme.colors.purple,
    width: 30,
    height: 30,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default style;
