import React, {forwardRef, useRef, useState} from 'react';
import {View,Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import * as theme from '../../utils/theme';
import Modal from 'react-native-modal';
import {WebView} from 'react-native-webview';
import {Spinner} from '..';
import styles from './style';
import Icon from 'react-native-vector-icons/Ionicons';
import { useDispatch, useSelector } from 'react-redux';
import { showNetworkInfoModalSelector } from '../../store/app/app.selectors';
import { closeNetworkInfoModal } from '../../store/app/app.actions';

function NetworkConnectionModal() {

  const dispatch = useDispatch()
  const visible = useSelector(showNetworkInfoModalSelector)

  function onClose(){
    dispatch(closeNetworkInfoModal())
  }
  return (
    <Modal
      isVisible={visible}
      useNativeDriver
      animationIn="pulse"
      animationInTiming={400}
      onBackdropPress={onClose}
      onBackButtonPress={onClose}
      style={styles.viewModal}>
      <View style={styles.content}>
        <Text style={styles.viewEmoji}>😲</Text>
        <Text style={styles.textHeader}>Oops...</Text>
        <Text style={styles.textSubHeader}>It looks like you don't have internet connection.</Text>
        <Text style={styles.textSubHeader}>You're in offline mode.</Text>

      </View>
    </Modal>
  );
}

export default NetworkConnectionModal;
