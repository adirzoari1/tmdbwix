import  Movies  from './Movies'
import SearchMovies from './SearchMovies'
import Watchlist from './Watchlist'


export { 
    Movies,
    SearchMovies,
    Watchlist
}