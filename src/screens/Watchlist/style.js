import { StyleSheet} from 'react-native';
import * as theme from '../../utils/theme'
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:theme.colors.black

  },
  viewEmpty: {
  
  },
  contentFlatList: {
    flexGrow: 1,
    padding:20
  },
  viewEmptyWatchlist:{
    justifyContent:'center',
    alignItems:'center',
    height:'100%',
  },
  textEmoji:{
    fontSize:60,
    marginBottom:10
  },
  textEmptyWatchlist:{
    fontFamily:theme.fonts.regular,
    color:theme.colors.white,
    fontSize:18
  },
  subTextEmptyWatchlist:{
    fontFamily:theme.fonts.light,
    color:theme.colors.white,
    fontSize:14,
  },
  textHeaderWatchlist:{
    fontFamily:theme.fonts.bold,
    color:theme.colors.white,
    fontSize:20,
  }
});

export default style;
