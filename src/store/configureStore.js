import {createStore, applyMiddleware} from 'redux';
import {persistStore} from 'redux-persist';
import {persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import {createFilter} from 'redux-persist-transform-filter';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

import createSagaMiddleware from 'redux-saga';

import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

const subsetMovieFilterReducer = createFilter(
  'movie',
  ['moviesResponse'],
);

export const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['movie', 'watchlist'],
  transforms: [subsetMovieFilterReducer],
  stateReconciler: autoMergeLevel2

};

const sagaMiddleware = createSagaMiddleware();
const persistedReducer = persistReducer(persistConfig, rootReducer);

const middlewares = [sagaMiddleware];
const store = createStore(persistedReducer, applyMiddleware(...middlewares));

sagaMiddleware.run(rootSaga);

const persistor = persistStore(store);
export {store, persistor};
