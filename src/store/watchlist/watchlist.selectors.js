import {createSelector} from 'reselect';
import {get} from 'lodash';

const baseWatchlist = state => state.watchlist;

const moviesWatchlistSelector = createSelector(baseWatchlist, baseWatch =>
  get(baseWatch, 'watchlist', []),
);
const moviesWatchlistIdsSelector = createSelector(baseWatchlist, baseWatch =>
  get(baseWatch, 'watchlistIds', {}),
);
export {moviesWatchlistSelector, moviesWatchlistIdsSelector};
