import Types from './watchlist.types';
const initialState = {
  watchlist: [],
  watchlistIds: {},
};

const watchlistReducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case Types.ADD_MOVIE_TO_WATCHLIST: {
      const {movie} = payload;
      
      return {
        ...state,
        watchlist: [...state.watchlist, movie],
        watchlistIds: {...state.watchlistIds, [movie.id]: true},
      };
    }
    case Types.REMOVE_MOVIE_FROM_WATCHLIST: {
      const {id} = payload;
      return {
        ...state,
        watchlist: state.watchlist.filter(movie => movie.id != id),
        watchlistIds: {...state.watchlistIds, [id]: false},
      };
    }

    default:
      return state;
  }
};

export default watchlistReducer;
