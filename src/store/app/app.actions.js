import Types from './app.types';

const setUserCountry = data => ({
  type: Types.SET_USER_COUNTRY,
  payload: data,
});

const setNetworkInfo = data => ({
  type: Types.SET_NETWORK_INFO,
  payload: data,
});

const closeNetworkInfoModal = () => ({
  type: Types.CLOSE_NETWORK_MODAL,
});

export {setUserCountry, setNetworkInfo, closeNetworkInfoModal};
