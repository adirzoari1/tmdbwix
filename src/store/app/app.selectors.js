import {createSelector} from 'reselect';
import {get} from 'lodash';

const baseAppSelector = state => state.app;

const networkInfoSelector = createSelector(
  baseAppSelector,
  baseApp => baseApp.networkInfo,
);

const showNetworkInfoModalSelector = createSelector(
  networkInfoSelector,
  networkInfo => networkInfo.showModalVisible,
);

const isConnectedSelector = createSelector(
  networkInfoSelector,
  networkInfo => networkInfo.isConnected,
);



const userCountrySelector = createSelector(baseAppSelector, baseApp =>
  get(baseApp, 'userCountry'),
);

export {userCountrySelector,networkInfoSelector,showNetworkInfoModalSelector,isConnectedSelector};
