import Types from './movie.types';

const getPopularMoviesRequest = data => ({
  type: Types.GET_POPULAR_MOVIES_REQUEST,
  payload: data,
});
const getPopularMoviesSuccess = data => ({
  type: Types.GET_POPULAR_MOVIES_SUCCESS,
  payload: data,
});
const getPopularMoviesError = error => ({
  type: Types.GET_POPULAR_MOVIES_FAILURE,
});


const getMovieInfoRequest = data => ({
    type: Types.GET_MOVIE_INFO_REQUEST,
    payload: data,
  });
  const getMovieInfoSuccess = data => ({
    type: Types.GET_MOVIE_INFO_SUCCESS,
    payload: data,
  });
  const getMovieInfoFailure = error => ({
    type: Types.GET_MOVIE_INFO_FAILURE,
  });


const searchMoviesRequest = data => ({
  type: Types.SEARCH_MOVIES_REQUEST,
  payload: data,
});
const searchMoviesSuccess = data => ({
  type: Types.SEARCH_MOVIES_SUCCESS,
  payload: data,
});
const searchMoviesFailure = data => ({
  type: Types.SEARCH_MOVIES_FAILURE,
  payload: data,
});

const resetSearch = () => ({type: Types.RESET_SEARCH});


export {
    getPopularMoviesRequest,
    getPopularMoviesSuccess,
    getPopularMoviesError,
    getMovieInfoRequest,
    getMovieInfoSuccess,
    getMovieInfoFailure,
    searchMoviesRequest,
    searchMoviesSuccess,
    searchMoviesFailure,
    resetSearch,

};
