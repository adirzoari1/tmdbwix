import Types from './movie.types';

export const initialMoviesResponse = {
  results: [],
  totalPages: 0,
  totalResults: 0,
  page:1,
};
const initialState = {
  moviesResponse: initialMoviesResponse,
  searchMoviesResponse: initialMoviesResponse,
  selectedWatchMovieInfo: null,
  isLoading: false,
};

const movieReducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case Types.GET_POPULAR_MOVIES_REQUEST:
    case Types.SEARCH_MOVIES_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case Types.GET_POPULAR_MOVIES_SUCCESS: {
      const {
        responseData: {results,page, ...rest},
      } = payload;
      const updatedMoviesResult = page === 1? [...results] : state.moviesResponse.results.concat(results) 
      return {
        ...state,
        moviesResponse: {
          ...rest,
          page,
          results:updatedMoviesResult
        },
        isLoading: false,
      };
    }
    case Types.GET_POPULAR_MOVIES_FAILURE: {
      return {
        ...state,
        movies: initialMoviesResponse,
        isLoading: false,
      };
    }

    case Types.SEARCH_MOVIES_SUCCESS: {
      const {
        responseData
      } = payload;

      return {
        ...state,
        searchMoviesResponse:{...responseData},
        isLoading: false,
      };
    }

    case Types.SEARCH_MOVIES_FAILURE: {
      return {
        ...state,
        searchMoviesResponse: initialMoviesResponse,
        isLoading: false,
      };
    }

  
    case Types.GET_MOVIE_INFO_SUCCESS: {
      const {selectedWatchMovieInfo} = payload;
      return {
        ...state,
        selectedWatchMovieInfo,
        isLoading: false,
      };
    }
    case Types.RESET_SEARCH:{
      return {
        ...state,
        searchMoviesResponse:initialMoviesResponse
      }
    }
 

    default:
      return state;
  }
};

export default movieReducer;
