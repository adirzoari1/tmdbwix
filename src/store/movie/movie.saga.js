import {takeLatest, put, all, call, select} from 'redux-saga/effects';
import {get} from 'lodash';
import Types from './movie.types';
import {getPopularMovies, getMovieInfo, searchMovies} from '../../utils/api';
import {
  getPopularMoviesSuccess,
  getPopularMoviesError,
  searchMoviesSuccess,
  searchMoviesFailure,
  getMovieInfoSuccess,
  getPopularMoviesRequest,
  searchMoviesRequest,
  getMovieInfoRequest,
  getMovieInfoFailure,
} from './movie.actions';
import { deviceLocalCode } from '../../utils/helpers';
import { userCountrySelector } from '../app/app.selectors';


function* getPopularMoviesSaga({payload, type}) {
  try {
    const { page = false } = payload
    const userCountry = yield select(userCountrySelector)
    const response = yield call(getPopularMovies, {page,region:userCountry});
    const responseData = {
      results: get(response, 'data.results', []),
      totalPages: get(response, 'data.total_pages', 0),
      totalResults: get(response, 'data.total_results', 0),
      page: get(response, 'data.page', 1),
    };

    yield put(getPopularMoviesSuccess({responseData}));
  } catch (error) {
    yield put(getPopularMoviesError(error));
  }
}

function* searchMoviesSaga({payload, type}) {
  try {

    const response = yield call(searchMovies, payload);
    const responseData = {
      results: get(response, 'data.results', []),
      totalPages: get(response, 'data.total_pages', 0),
      totalResults: get(response, 'data.total_results', 0),
      page: get(response, 'data.page', 1),
    };

    yield put(searchMoviesSuccess({responseData}));
  } catch (error) {
    yield put(searchMoviesFailure(error));
  }
}

function* getMovieInfoSaga({payload}) {
  try {
    const response = yield call(getMovieInfo, payload);
    let responseData = {
      selectedWatchMovieInfo: {
        ...response.data,
        trailerId: get(response, 'data.videos.results.[0].key', null),
      },
    };
    yield put(getMovieInfoSuccess(responseData));
  } catch (error) {
    yield put(getMovieInfoFailure())

  }
}

function* movieSaga() {
  yield all([
    takeLatest(Types.GET_POPULAR_MOVIES_REQUEST, getPopularMoviesSaga),
    takeLatest(Types.GET_MOVIE_INFO_REQUEST, getMovieInfoSaga),
    takeLatest(Types.SEARCH_MOVIES_REQUEST, searchMoviesSaga),
  ]);
}

export default movieSaga;
