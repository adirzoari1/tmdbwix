import React from 'react';
import {StatusBar, View} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {Spinner} from './components';
import RootNavigator from './navigation';
import {store, persistor} from './store/configureStore';
import * as theme from './utils/theme';

function App() {
  return (
    <Provider store={store}>
      <PersistGate
        loading={<Spinner color={theme.colors.black} />}
        persistor={persistor}>
        <View style={{flex: 1}}>
          <StatusBar backgroundColor={theme.colors.black}  />
          <RootNavigator />
        </View>
      </PersistGate>
    </Provider>
  );
}

export default App;
